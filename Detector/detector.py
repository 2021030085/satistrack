import cv2
from tensorflow.keras.models import load_model
import numpy as np

# Definir los nombres de las emociones
emotion_labels = ["anger", "disgust", "fear", "happy", "sadness", "surprise", "contempt"]

# Cargar el modelo entrenado
model = load_model("emotion_recognition_model.h5")

# Iniciar la captura de video desde la webcam
cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()
    if not ret:
        break

    # Preprocesar el frame si es necesario
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    resized = cv2.resize(gray, (48, 48))
    normalized = resized / 255.0
    reshaped = np.reshape(normalized, (1, 48, 48, 1))

    # Realizar la predicción con el modelo
    prediction = model.predict(reshaped)
    emotion_label = emotion_labels[np.argmax(prediction)]

    # Mostrar la emoción predicha en el frame
    cv2.putText(frame, "Emotion: {}".format(emotion_label), (10, 30),
                cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 255, 0), 2)

    # Mostrar el frame en una ventana
    cv2.imshow('Emotion Detection', frame)

    # Salir del bucle si se presiona la tecla 'q'
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Liberar la captura de video y cerrar la ventana
cap.release()
cv2.destroyAllWindows()
