import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import os

# Ruta al dataset de imágenes de emociones
dataset_path = "C:/Users/Jorge/Desktop/Detector/Emociones"

# Lista de las emociones disponibles en el dataset
emotions = ["anger", "disgust", "fear", "happy", "sadness", "surprise", "contempt"]

# Configurar el generador de imágenes
datagen = ImageDataGenerator(rescale=1./255)
train_generator = datagen.flow_from_directory(
        dataset_path,
        target_size=(48, 48),
        batch_size=32,
        class_mode='categorical',
        color_mode='grayscale',
        classes=emotions)  # Asegúrate de incluir todas las clases en el generador de imágenes

# Construir el modelo
model = tf.keras.Sequential([
    tf.keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=(48, 48, 1)),
    tf.keras.layers.MaxPooling2D((2, 2)),
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D((2, 2)),
    tf.keras.layers.Conv2D(128, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D((2, 2)),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(7, activation='softmax')  # Cambiado a 7 para ajustar el número de clases
])

# Compilar el modelo
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

# Entrenar el modelo
model.fit(train_generator, epochs=10)

# Guardar el modelo
model.save("emotion_recognition_model.h5")
